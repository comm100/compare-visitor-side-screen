


var state = {
    "app": {
        "isReady": true,
        "isActive": true,
        "server": "main",
        "isPoppedOut": false,
        "isOpenerExists": false
    },
    "page": {
        "siteId": Comm100API.site_id,
        "isDynamicCampaign": false,
        "campaignId": Comm100API.campaign_id,
        "buttons": {
            [Comm100API.campaign_id]: true
        },
        "platform": "desktop",
        "visitor": {
            "id": 241,
            "standbyId": null,
            "guid": "-9xxqA39H0CiaApkeS57Tw"
        },
        "windowStatus": "open",
        "requestChatAction": {
            "type": "requestChat",
            "campaignId": Comm100API.campaign_id,
            "source": {
                "type": "button",
                "page": {
                    "title": "Preview",
                    "url": "https://ent.comm100.com/LiveChatFunc/PlanPreview.aspx?codePlanId=1433&SSL=1&siteid=1000008"
                }
            },
            "isPrechatDone": false,
            "isSupportWebrtc": true
        },
        "title": "Preview",
        "url": "https://ent.comm100.com/LiveChatFunc/PlanPreview.aspx?codePlanId=1433&SSL=1&siteid=1000008",
        "width": 883,
        "height": 930,
        "orientation": "landscape",
        "invitation": null
    },
    "entities": {
        "agents": {
            "entities": {
                "1": {
                    "id": 1,
                    "name": "onlineenttest1",
                    "title": "aaaaa",
                    "avatar": "https://ent.comm100.com/AdminManage/AdminPanel/operatoravatarforvm.aspx?siteId=1000008&operatorId=1&v=636692650484849153",
                    "bio": "<div>dafadfa</div>",
                    "index": 0
                },
                "16": {
                    "id": 16,
                    "name": "carl",
                    "title": "",
                    "avatar": "https://ent.comm100.com/AdminManage/images/SystemAvatar/121.png",
                    "bio": "",
                    "index": 1
                },
                "39": {
                    "id": 39,
                    "name": "comm100testlead@comm100.com",
                    "title": "",
                    "avatar": "https://ent.comm100.com/AdminManage/images/SystemAvatar/103.png",
                    "bio": "",
                    "index": 2
                }
            },
            "errors": {},
            "fetchStatus": {
                "1": "done",
                "16": "done",
                "39": "done"
            }
        },
        "messages": {
            "entities": {},
            "errors": {},
            "fetchStatus": {}
        },
        "buttons": {
            "entities": {
                [Comm100API.campaign_id]: {
                    "position": {
                        "type": "bottomMiddle",
                        "xoffset": {
                            "percent": 0
                        },
                        "yoffset": {
                            "percent": 0
                        }
                    },
                    "imageOnline": "https://ent.comm100.com/chatserver/DBResource/DBImage.ashx?imgId=2280&type=2&siteId=1000008",
                    "imageOffline": "https://ent.comm100.com/chatserver/DBResource/DBImage.ashx?imgId=2281&type=2&siteId=1000008",
                    "id": Comm100API.campaign_id,
                    "type": "float",
                    "isHideOffline": false,
                    "isEmbeddedWindow": true,
                    "customOffline": null,
                    "lastUpdateTime": "FF4F099ECDB1E9D2D690736AA09A79772218743B8B347C46CFC4B1E9AE85F97C",
                    "routeDepartment": -1,
                    "windowStyle": "bubble",
                    "divId": "comm100-button-1433"
                }
            },
            "errors": {},
            "fetchStatus": {
                "1433": "done"
            }
        },
        "campaigns": {
            "entities": {
            },
            "errors": {},
            "fetchStatus": {
            }
        },
        "fields": {
            "entities": {
            },
            "errors": {},
            "fetchStatus": {
            }
        },
        "departments": {
            "entities": {
                "1": {
                    "name": "Department1",
                    "id": 1,
                    "isOnline": true
                },
                "688": {
                    "name": "D2",
                    "id": 688,
                    "isOnline": false
                },
                "704": {
                    "name": "SALES DEPARTMENT",
                    "id": 704,
                    "isOnline": false
                },
                "748": {
                    "name": "SalesA",
                    "id": 748,
                    "isOnline": false
                },
                "749": {
                    "name": "SalesB",
                    "id": 749,
                    "isOnline": false
                },
                "750": {
                    "name": "AfterSaleC",
                    "id": 750,
                    "isOnline": false
                },
                "830": {
                    "name": "ad1231d4",
                    "id": 830,
                    "isOnline": false
                },
                "831": {
                    "name": "D1",
                    "id": 831,
                    "isOnline": true
                },
                "832": {
                    "name": "D3",
                    "id": 832,
                    "isOnline": false
                },
                "833": {
                    "name": "D123",
                    "id": 833,
                    "isOnline": false
                },
                "834": {
                    "name": "test department",
                    "id": 834,
                    "isOnline": false
                },
                "1483": {
                    "name": "a1111114",
                    "id": 1483,
                    "isOnline": false
                },
                "1484": {
                    "name": "a11",
                    "id": 1484,
                    "isOnline": false
                },
                "1498": {
                    "name": "testdepartment",
                    "id": 1498,
                    "isOnline": true
                },
                "1528": {
                    "name": "emptydepartment",
                    "id": 1528,
                    "isOnline": false
                }
            },
            "errors": {},
            "fetchStatus": {
                "1": "done",
                "688": "done",
                "704": "done",
                "748": "done",
                "749": "done",
                "750": "done",
                "830": "done",
                "831": "done",
                "832": "done",
                "833": "done",
                "834": "done",
                "1483": "done",
                "1484": "done",
                "1498": "done",
                "1528": "done"
            }
        },
        "kbArticles": {
            "entities": {},
            "errors": {},
            "fetchStatus": {}
        }
    },
    "window": {
        "fields": {
            "name": "",
            "email": "",
            "phone": "",
            "company": "",
            "product": "",
            "department": -1,
            "custom": {}
        },
        "forms": {
            "prechat": "none",
            "postchat": "none",
            "offline": "none"
        },
        "chatStatus": "none",
        "messages": [],
        "maxId": -1,
        "involvedAgents": [],
        "draft": "",
        "maxReadId": -1,
        "isFocused": true,
        "isFrequencyLimited": false,
        "showPrelogin": false,
        "ssoUrl": "",
        "queue": {},
        "error": "",
        "warning": "",
        "fileUploadCount": 0,
        "disabled": false,
        "vChatStatus": "notStart",
        "audioVideoChatStartTime": 0,
        "stopLimitClocker": false,
        "limitClockerCount": 0,
        "popupBubble": false,
        "cobrowsing": "none",
        "cobrowseAgent": 0,
        "id": Comm100API.campaign_id,
        "tab": "prechat"
    },
    "configuration": {
        "url": "https://ent.comm100.com/chatserver",
        "customVariableResult": {
            "WebBrowser": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36",
            "host": "ent.comm100.com",
            "href": "https://ent.comm100.com/LiveChatFunc/PlanPreview.aspx?codePlanId=1433&SSL=1&siteid=1000008"
        },
        "customVariables": {
            "WebBrowser": "navigator.userAgent",
            "host": "window.location.host",
            "text1": "text1",
            "int0": "int0",
            "decimal1": "decimal1",
            "href": "window.location.href"
        },
        "facebookAppId": "1436722786610570",
        "googlePlusAppId": "947868477424-vpsrbffgr4nra6au625ohv56cirflebh.apps.googleusercontent.com",
        "attachmentMaxSize": 5,
        "attachmentMaxCount": 10,
        "frequencyLimit": {
            "duration": 5,
            "count": 5,
            "penalty": 30,
            "repeat": 3
        },
        "heartbeat": {
            "online": [
                {
                    "duration": 60,
                    "timespan": 300
                },
                {
                    "duration": 80,
                    "timespan": 300
                },
                {
                    "duration": 80,
                    "timespan": 1800
                }
            ],
            "offline": 160,
            "stopAfter": 600
        },
        "isChinaIP": true,
        "features": {
            "ga": true,
            "department": true,
            "hideCreditCard": true,
            "showTypingContent": true,
            "showChatMessageTime": true,
            "useSessionCookie": false,
            "heartbeat": true,
            "sendFile": true,
            "autoInvitation": true,
            "hideAgentName": false,
            "disableCookie": false,
            "audioVideoChat": true,
            "removePoweredby": true,
            "ticket": false
        },
        "kbAPIUrl": "https://ent.comm100.com/kbwebapi",
        "kbClientUrl": "https://ent.comm100.com/kb",
        "shopifyAppApiUrl": "https://ent.comm100.com/ShopifyApp",
        "messageLengthLimit": 10000,
        "webrtcSignalingServiceUrl": "https://ent.comm100.com",
        "downloadUrl": "https://ent.comm100download.com/DownloadFile/download.ashx"
    }
}

function updateStateByCampaign(result, ifCustomCss) {
    var campaignData = JSON.parse(JSON.stringify(result));
    // console.log(campaignData);
    state.entities.campaigns.entities[result.id] = campaignData;
    state.entities.campaigns.fetchStatus[result.id] = "done";
    state.entities.campaigns.entities[result.id].prechat = [];
    state.entities.campaigns.entities[result.id].offline = [];
    state.entities.campaigns.entities[result.id].postchat = [];

    state.window.id = result.id;
    // state.window.tab = "prechat";
    state.entities.campaigns.entities[result.id].chat.texture = Comm100API.chatserver_url+"/" + result.chat.texture;
    state.entities.campaigns.entities[result.id].header.banner = Comm100API.chatserver_url + result.header.banner;
    state.entities.campaigns.entities[result.id].sounds = {
        "new-response": Comm100API.chatserver_url + result.sounds["new-response"],
        "video-request": Comm100API.chatserver_url + result.sounds['video-request'],
        "video-end": Comm100API.chatserver_url + result.sounds['video-end']
    }

    result.perchat && result.prechat.forEach(element => {
        state.entities.fields.entities[element.id] = element;
        state.entities.fields.fetchStatus[element.id] = "done";
        state.entities.campaigns.entities[result.id].prechat.push(element.id);
    });
    result.offline && result.offline.forEach(element => {
        state.entities.fields.entities[element.id] = element;
        state.entities.fields.fetchStatus[element.id] = "done";
        state.entities.campaigns.entities[result.id].offline.push(element.id);
    });
    result.postchat && result.postchat.forEach(element => {
        state.entities.fields.entities[element.id] = element;
        state.entities.fields.fetchStatus[element.id] = "done";
        state.entities.campaigns.entities[result.id].postchat.push(element.id);
    });

    if (!ifCustomCss) {
        state.entities.campaigns.entities[result.id].customCSS = ""; 
    }

    render(state);
}
