const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer');
// const sum = require('./sum')

// test('adds 1 + 2 to equal 3', () => {
//     expect(sum(1, 2)).toBe(3);
//   });

const sleep = ms => new Promise(resolve => {
  setTimeout(resolve, ms);  
});

describe('postchat image', () => {
  let browser;

  beforeAll(async () => {
    browser = await puppeteer.launch({
      executablePath: './chrome-win32/chrome.exe',
      headless: false
    });
  });

  it('works', async () => {
    
    let page = await browser.newPage();
    await page.goto('http://localhost:8010/page/postchat.html');
    await sleep(4000);
    await page.screenshot({path: './test/__image_snapshots__/postchat-test-js-postchat-image-works-1-snap.png'});
    // expect(image).toMatchImageSnapshot();

    await page.goto('http://localhost:8010/page/postchat_CSS.html');
    await sleep(4000);
    const image = await page.screenshot({path: './test/postchat.png'});
    expect(image).toMatchImageSnapshot();

  },13100);

  afterAll(async () => {
    await browser.close();
  });
});

// describe('Test', () => {
//   test('timeout', async () => {
//     await new Promise(resolve => setTimeout(resolve, 5000));
//   }, 6000);
// });

  // it('works reading an image from the local file system', () => {
  //   const imageAtTestPath = path.resolve(__dirname, './stubs', 'image.png');
  //   // imageAtTest is a PNG encoded image buffer which is what `toMatchImageSnapshot() expects
  //   const imageAtTest = fs.readFileSync(imageAtTestPath);
  
  //   expect(imageAtTest).toMatchImageSnapshot();
  // });

